import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  loadUsers() : Observable<any> {
    return this.httpClient.get('https://jsonplaceholder.typicode.com/users')
  }
}
