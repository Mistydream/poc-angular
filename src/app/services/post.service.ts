import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../models/post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpClient: HttpClient) { }

  loadPosts() : Observable<any> {
    return this.httpClient.get('https://jsonplaceholder.typicode.com/posts');
  }

  sendPost(post: Post) : Observable<any> {
    return this.httpClient.post('https://jsonplaceholder.typicode.com/posts', {
        title: post.title,
        body: post.body,
        userId: post.userId,
      });
  }

  updatePost(post: Post) : Observable<any> {
    return this.httpClient.put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
      title: post.title,
      body: post.body,
    });
  }
}
