import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.css']
})
export class PostPageComponent implements OnInit {
  newPost: object;

  constructor() { }

  ngOnInit() {
    this.newPost = null;
  }

  newPostCreate(post) {
    this.newPost = post;
  }
}
