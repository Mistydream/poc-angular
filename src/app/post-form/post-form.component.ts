import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { PostService } from '../services/post.service';
import { Post } from '../models/post.interface';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Input()
  data: Post;

  @Output()
  newPost = new EventEmitter<Object>();
  
  postForm: FormGroup;

  constructor(private fb: FormBuilder, private postService: PostService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.postForm = this.fb.group({
      id: this.data ? this.data.id : null,
      title: this.data ? this.data.title : '',
      body: this.data ? this.data.body : '',
      userId: this.data ? this.data.userId : 1,
    });
  }

  onSubmit() {
    if (this.data){
      this.postService.updatePost(this.postForm.value).subscribe(data => this.newPost.emit(data));
    } else {
      this.postService.sendPost(this.postForm.value).subscribe(data => this.newPost.emit(data));
    }
  }

}
