import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @Input()
    users: [];

  @Output()
    selectUser = new EventEmitter<number>()

  constructor() { }

  ngOnInit() {
  }

  onClick(userId) {
    this.selectUser.emit(userId);
  }

}
