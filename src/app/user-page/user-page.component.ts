import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { UserService } from '../services/user.service';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  users: [] = [];
  todos: [] = [];
  userId: number = 1;

  constructor(private userService: UserService, private todoService: TodoService) { }

  ngOnInit() {
    this.userService.loadUsers().subscribe(data => this.users = data);
    this.todoService.loadTodosByUser(this.userId)
      .pipe(
        map(todos => {
          const list = todos.filter(todo => todo.userId === this.userId);
          return list;
        })
      )
      .subscribe(data => this.todos = data);
  }

  userClick(event) {
    this.todoService.loadTodosByUser(event)
    .pipe(
      map(todos => {
        const list = todos.filter(todo => todo.userId === event);
        return list;
      })
    )
    .subscribe(data => this.todos = data);
  }

}
