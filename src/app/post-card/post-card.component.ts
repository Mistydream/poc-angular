import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  newPost: object;
  isEdit: boolean;

  @Input()
  post: Object

  constructor() { }

  ngOnInit() {
    this.newPost = null;
    this.isEdit = false;
  }

  onEdit() {
    this.isEdit = true;
  }

  newPostUpdate(post) {
    this.post = post;
    this.isEdit = false;
  }
}
