import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../services/post.service';

import { Post } from '../models/post.interface';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  @Input()
  set newPost(post: never) {
    if (post) {
      this.posts.unshift(post);
    }
  };

  posts: [] = [];

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.loadPosts().subscribe(data => this.posts = data);
  }

}
